# Diff de las legislaciones federales vigentes de México

| Directorio | Legislación |
|---|---|
| [001.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/001.md) | Constitución Política de los Estados Unidos Mexicanos |
| [002.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/002.md) | Código Civil Federal |
| [003.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/003.md) | Código de Comercio |
| [004.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/004.md) | Código de Justicia Militar |
| [005.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/005.md) | Código Federal de Procedimientos Civiles |
| [006.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/006.md) | Código Fiscal de la Federación |
| [007.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/007.md) | Código Militar de Procedimientos Penales |
| [008.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/008.md) | Código Nacional de Procedimientos Penales |
| [009.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/009.md) | Código Penal Federal |
| [010.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/010.md) | Estatuto de Gobierno del Distrito Federal |
| [011.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/011.md) | Impuesto sobre Servicios Expresamente Declarados de Interés Público por Ley, en los que Intervengan Empresas Concesionarias de Bienes del Dominio Directo de la Nación (LEY que establece, reforma y adiciona las disposiciones relativas a diversos impuestos) |
| [012.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/012.md) | Ley Aduanera |
| [013.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/013.md) | Ley Agraria |
| [014.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/014.md) | Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público |
| [015.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/015.md) | Ley de Aeropuertos |
| [016.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/016.md) | Ley de Aguas Nacionales |
| [017.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/017.md) | Ley de Ahorro y Crédito Popular |
| [018.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/018.md) | Ley de Amnistía |
| [019.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/019.md) | Ley de Amnistía |
| [020.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/020.md) | Ley de Amparo, Reglamentaria de los artículos 103 y 107 de la Constitución Política de los Estados Unidos Mexicanos |
| [021.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/021.md) | Ley de Ascensos de la Armada de México |
| [022.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/022.md) | Ley de Ascensos y Recompensas del Ejército y Fuerza Aérea Mexicanos |
| [023.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/023.md) | Ley de Asistencia Social |
| [024.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/024.md) | Ley de Asociaciones Público Privadas |
| [025.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/025.md) | Ley de Asociaciones Religiosas y Culto Público |
| [026.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/026.md) | Ley de Aviación Civil |
| [027.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/027.md) | Ley de Ayuda Alimentaria para los Trabajadores |
| [028.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/028.md) | Ley de Bioseguridad de Organismos Genéticamente Modificados |
| [029.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/029.md) | Ley de Cámaras Empresariales y sus Confederaciones |
| [030.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/030.md) | Ley de Caminos, Puentes y Autotransporte Federal |
| [031.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/031.md) | Ley de Capitalización del Procampo |
| [032.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/032.md) | Ley de Carrera Judicial del Poder Judicial de la Federación |
| [033.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/033.md) | Ley de Ciencia y Tecnología |
| [034.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/034.md) | Ley de Comercio Exterior |
| [035.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/035.md) | Ley de Concursos Mercantiles |
| [036.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/036.md) | Ley de Contribución de Mejoras por Obras Públicas Federales de Infraestructura Hidráulica |
| [037.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/037.md) | Ley de Cooperación Internacional para el Desarrollo |
| [038.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/038.md) | Ley de Coordinación Fiscal |
| [039.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/039.md) | Ley de Desarrollo Rural Sustentable |
| [040.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/040.md) | Ley de Desarrollo Sustentable de la Caña de Azúcar |
| [041.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/041.md) | Ley de Disciplina del Ejército y Fuerza Aérea Mexicanos |
| [042.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/042.md) | Ley de Disciplina Financiera de las Entidades Federativas y los Municipios |
| [043.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/043.md) | Ley de Disciplina para el Personal de la Armada de México |
| [044.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/044.md) | Ley de Educación Militar del Ejército y Fuerza Aérea Mexicanos |
| [045.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/045.md) | Ley de Educación Naval |
| [046.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/046.md) | Ley de Energía Geotérmica |
| [047.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/047.md) | Ley de Energía para el Campo |
| [048.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/048.md) | Ley de Expropiación |
| [049.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/049.md) | Ley de Extradición Internacional |
| [050.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/050.md) | Ley de Firma Electrónica Avanzada |
| [051.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/051.md) | Ley de Fiscalización y Rendición de Cuentas de la Federación |
| [052.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/052.md) | Ley de Fomento a la Confianza Ciudadana |
| [053.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/053.md) | Ley de Fomento a la Industria Vitivinícola |
| [054.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/054.md) | Ley de Fomento para la Lectura y el Libro |
| [055.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/055.md) | Ley de Fondos de Aseguramiento Agropecuario y Rural |
| [056.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/056.md) | Ley de Fondos de Inversión |
| [057.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/057.md) | Ley de Hidrocarburos |
| [058.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/058.md) | Ley de Infraestructura de la Calidad |
| [059.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/059.md) | Ley de Ingresos de la Federación para el Ejercicio Fiscal de 2022 |
| [060.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/060.md) | Ley de Ingresos de la Federación para el Ejercicio Fiscal de 2021 |
| [061.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/061.md) | Ley de Ingresos sobre Hidrocarburos |
| [062.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/062.md) | Ley de Instituciones de Crédito |
| [063.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/063.md) | Ley de Instituciones de Seguros y de Fianzas |
| [064.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/064.md) | Ley de Inversión Extranjera |
| [065.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/065.md) | Ley de la Agencia Nacional de Seguridad Industrial y de Protección al Medio Ambiente del Sector Hidrocarburos |
| [066.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/066.md) | Ley de la Casa de Moneda de México |
| [067.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/067.md) | Ley de la Comisión Federal de Electricidad |
| [068.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/068.md) | Ley de la Comisión Nacional Bancaria y de Valores |
| [069.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/069.md) | Ley de la Comisión Nacional de los Derechos Humanos |
| [070.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/070.md) | Ley de la Economía Social y Solidaria, Reglamentaria del Párrafo Octavo del Artículo 25 de la Constitución Política de los Estados Unidos Mexicanos, en lo referente al sector social de la economía |
| [071.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/071.md) | Ley de la Fiscalía General de la República |
| [072.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/072.md) | Ley de la Guardia Nacional |
| [073.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/073.md) | Ley de la Industria Eléctrica |
| [074.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/074.md) | Ley de la Policía Federal |
| [075.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/075.md) | Ley de los Derechos de las Personas Adultas Mayores |
| [076.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/076.md) | Ley de los Impuestos Generales de Importación y de Exportación |
| [077.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/077.md) | Ley de los Institutos Nacionales de Salud |
| [078.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/078.md) | Ley de los Órganos Reguladores Coordinados en Materia Energética |
| [079.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/079.md) | Ley de los Sistemas de Ahorro para el Retiro |
| [080.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/080.md) | Ley de  Migración |
| [081.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/081.md) | Ley de Nacionalidad |
| [082.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/082.md) | Ley de Navegación y Comercio Marítimos |
| [083.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/083.md) | Ley de Obras Públicas y Servicios Relacionados con las Mismas |
| [084.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/084.md) | Ley de Organizaciones Ganaderas |
| [085.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/085.md) | Ley de Petróleos Mexicanos |
| [086.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/086.md) | Ley de Planeación |
| [087.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/087.md) | Ley de Premios, Estímulos y Recompensas Civiles |
| [088.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/088.md) | Ley de Productos Orgánicos |
| [089.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/089.md) | Ley de Promoción y Desarrollo de los Bioenergéticos |
| [090.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/090.md) | Ley de Protección al Ahorro Bancario |
| [091.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/091.md) | Ley de Protección al Comercio y la Inversión de Normas Extranjeras que Contravengan el Derecho Internacional |
| [092.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/092.md) | Ley de Protección y Defensa al Usuario de Servicios Financieros |
| [093.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/093.md) | Ley de Puertos |
| [094.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/094.md) | Ley de Recompensas de la Armada de México |
| [095.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/095.md) | Ley de Responsabilidad Civil por Daños Nucleares |
| [096.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/096.md) | Ley de Seguridad Interior |
| [097.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/097.md) | Ley de Seguridad Nacional |
| [098.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/098.md) | Ley de Sistemas de Pagos |
| [099.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/099.md) | Ley de Sociedades de Responsabilidad Limitada de Interés Público |
| [100.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/100.md) | Ley de Sociedades de Solidaridad Social |
| [101.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/101.md) | Ley de Tesorería de la Federación |
| [102.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/102.md) | Ley de Transición Energética |
| [103.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/103.md) | Ley de Transparencia y de Fomento a la Competencia en el Crédito Garantizado |
| [104.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/104.md) | Ley de Uniones de Crédito |
| [105.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/105.md) | Ley de Vertimientos en las Zonas Marinas Mexicanas |
| [106.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/106.md) | Ley de Vías Generales de Comunicación |
| [107.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/107.md) | Ley de Vivienda |
| [108.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/108.md) | Ley del Banco de México |
| [109.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/109.md) | Ley del Diario Oficial de la Federación y Gacetas Gubernamentales |
| [110.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/110.md) | Ley del Fondo Mexicano del Petróleo para la Estabilización y el Desarrollo |
| [111.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/111.md) | Ley del Impuesto al Valor Agregado |
| [112.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/112.md) | Ley del Impuesto Especial sobre Producción y Servicios |
| [113.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/113.md) | Ley del Impuesto sobre la Renta |
| [114.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/114.md) | Ley del Instituto de Seguridad Social para las Fuerzas Armadas Mexicanas |
| [115.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/115.md) | Ley del Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado |
| [116.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/116.md) | Ley del Instituto del Fondo Nacional de la Vivienda para los Trabajadores |
| [117.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/117.md) | Ley del Instituto del Fondo Nacional para el Consumo de los Trabajadores |
| [118.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/118.md) | Ley del Instituto Mexicano de la Juventud |
| [119.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/119.md) | Ley del Instituto Nacional de las Mujeres |
| [120.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/120.md) | Ley del Instituto Nacional de los Pueblos Indígenas |
| [121.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/121.md) | Ley del Mercado de Valores |
| [122.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/122.md) | Ley del Registro Público Vehicular |
| [123.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/123.md) | Ley del Seguro Social |
| [124.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/124.md) | Ley del Servicio de Administración Tributaria |
| [125.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/125.md) | Ley del Servicio Exterior Mexicano |
| [126.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/126.md) | Ley del Servicio Militar |
| [127.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/127.md) | Ley del Servicio Postal Mexicano |
| [128.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/128.md) | Ley del Servicio Profesional de Carrera en la Administración Pública Federal |
| [129.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/129.md) | Ley del Sistema de Horario en los Estados Unidos Mexicanos |
| [130.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/130.md) | Ley del Sistema Nacional de Información Estadística y Geográfica |
| [131.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/131.md) | Ley del Sistema Público de Radiodifusión del Estado Mexicano |
| [132.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/132.md) | Ley en Favor de los Veteranos de la Revolución como Servidores del Estado |
| [133.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/133.md) | Ley Federal contra la Delincuencia Organizada |
| [134.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/134.md) | Ley Federal de Armas de Fuego y Explosivos |
| [135.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/135.md) | Ley Federal de Austeridad Republicana |
| [136.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/136.md) | Ley Federal de Cinematografía |
| [137.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/137.md) | Ley Federal de Competencia Económica |
| [138.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/138.md) | Ley Federal de Consulta Popular |
| [139.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/139.md) | Ley Federal de Correduría Pública |
| [140.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/140.md) | Ley Federal de Declaración Especial de Ausencia para Personas Desaparecidas |
| [141.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/141.md) | Ley Federal de Defensoría Pública |
| [142.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/142.md) | Ley Federal de Derechos |
| [143.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/143.md) | Ley Federal de Deuda Pública |
| [144.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/144.md) | Ley Federal de Fomento a las Actividades Realizadas por Organizaciones de la Sociedad Civil |
| [145.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/145.md) | Ley Federal de Juegos y Sorteos |
| [146.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/146.md) | Ley Federal de las Entidades Paraestatales |
| [147.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/147.md) | Ley Federal de los Derechos del Contribuyente |
| [148.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/148.md) | Ley Federal de los Trabajadores al Servicio del Estado, Reglamentaria del Apartado B) del Artículo 123 Constitucional |
| [149.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/149.md) | Ley Federal de Presupuesto y Responsabilidad Hacendaria |
| [150.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/150.md) | Ley Federal de Procedimiento Administrativo |
| [151.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/151.md) | Ley Federal de Procedimiento Contencioso Administrativo |
| [152.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/152.md) | Ley Federal de Producción, Certificación y Comercio de Semillas |
| [153.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/153.md) | Ley Federal de Protección a la Propiedad Industrial |
| [154.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/154.md) | Ley Federal de Protección al Consumidor |
| [155.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/155.md) | Ley Federal de Protección de Datos Personales en Posesión de los Particulares |
| [156.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/156.md) | Ley Federal de Remuneraciones de los Servidores Públicos |
| [157.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/157.md) | Ley Federal de Responsabilidad Ambiental |
| [158.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/158.md) | Ley Federal de Responsabilidad Patrimonial del Estado |
| [159.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/159.md) | Ley Federal de Responsabilidades de los Servidores Públicos |
| [160.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/160.md) | Ley Federal de Revocación de Mandato |
| [161.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/161.md) | Ley Federal de Sanidad Animal |
| [162.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/162.md) | Ley Federal de Sanidad Vegetal |
| [163.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/163.md) | Ley Federal de Seguridad Privada |
| [164.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/164.md) | Ley Federal de Telecomunicaciones y Radiodifusión |
| [165.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/165.md) | Ley Federal de Transparencia y Acceso a la Información Pública |
| [166.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/166.md) | Ley Federal de Variedades Vegetales |
| [167.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/167.md) | Ley Federal de Zonas Económicas Especiales |
| [168.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/168.md) | Ley Federal del Derecho de Autor |
| [169.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/169.md) | Ley Federal del Impuesto sobre Automóviles Nuevos |
| [170.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/170.md) | Ley Federal del Mar |
| [171.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/171.md) | Ley Federal del Trabajo |
| [172.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/172.md) | Ley Federal para el Control de Precursores Químicos, Productos Químicos Esenciales y Máquinas para Elaborar Cápsulas, Tabletas y/o Comprimidos |
| [173.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/173.md) | Ley Federal para el Control de Sustancias Químicas Susceptibles de Desvío para la Fabricación de Armas Químicas |
| [174.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/174.md) | Ley Federal para el Fomento de la Microindustria y la Actividad Artesanal |
| [175.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/175.md) | Ley Federal para el Fomento y Protección del Maíz Nativo |
| [176.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/176.md) | Ley Federal para la Administración y Enajenación de Bienes del Sector Público |
| [177.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/177.md) | Ley Federal para la Prevención e Identificación de Operaciones con Recursos de Procedencia Ilícita |
| [178.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/178.md) | Ley Federal para la Protección a Personas que Intervienen en el Procedimiento Penal |
| [179.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/179.md) | Ley Federal para Prevenir y Eliminar la Discriminación |
| [180.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/180.md) | Ley Federal para Prevenir y Sancionar los Delitos Cometidos en Materia de Hidrocarburos |
| [181.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/181.md) | Ley Federal sobre Monumentos y Zonas Arqueológicos, Artísticos e Históricos |
| [182.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/182.md) | Ley General de Acceso de las Mujeres a una Vida Libre de Violencia |
| [183.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/183.md) | Ley General de Archivos |
| [184.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/184.md) | Ley General de Asentamientos Humanos, Ordenamiento Territorial y Desarrollo Urbano |
| [185.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/185.md) | Ley General de Bibliotecas |
| [186.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/186.md) | Ley General de Bienes Nacionales |
| [187.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/187.md) | Ley General de Cambio Climático |
| [188.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/188.md) | Ley General de Comunicación Social |
| [189.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/189.md) | Ley General de Contabilidad Gubernamental |
| [190.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/190.md) | Ley General de Cultura Física y Deporte |
| [191.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/191.md) | Ley General de Cultura y Derechos Culturales |
| [192.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/192.md) | Ley General de Derechos Lingüísticos de los Pueblos Indígenas |
| [193.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/193.md) | Ley General de Desarrollo Forestal Sustentable |
| [194.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/194.md) | Ley General de Desarrollo Social |
| [195.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/195.md) | Ley General de Educación |
| [196.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/196.md) | Ley General de Educación Superior |
| [197.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/197.md) | Ley General de Instituciones y Procedimientos Electorales |
| [198.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/198.md) | Ley General de los Derechos de Niñas, Niños y Adolescentes |
| [199.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/199.md) | Ley General de Mejora Regulatoria |
| [200.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/200.md) | Ley General de Organizaciones y Actividades Auxiliares del Crédito |
| [201.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/201.md) | Ley General de Partidos Políticos |
| [202.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/202.md) | Ley General de Pesca y Acuacultura Sustentables |
| [203.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/203.md) | Ley General de Población |
| [204.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/204.md) | Ley General de Prestación de Servicios para la Atención, Cuidado y Desarrollo Integral Infantil |
| [205.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/205.md) | Ley General de Protección Civil |
| [206.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/206.md) | Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados |
| [207.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/207.md) | Ley General de Responsabilidades Administrativas |
| [208.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/208.md) | Ley General de Salud |
| [209.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/209.md) | Ley General de Sociedades Cooperativas |
| [210.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/210.md) | Ley General de Sociedades Mercantiles |
| [211.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/211.md) | Ley General de Títulos y Operaciones de Crédito |
| [212.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/212.md) | Ley General de Transparencia y Acceso a la Información Pública |
| [213.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/213.md) | Ley General de Turismo |
| [214.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/214.md) | Ley General de Víctimas |
| [215.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/215.md) | Ley General de Vida Silvestre |
| [216.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/216.md) | Ley General del Equilibrio Ecológico y la Protección al Ambiente |
| [217.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/217.md) | Ley General del Sistema de Medios de Impugnación en Materia Electoral |
| [218.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/218.md) | Ley General del Sistema Nacional Anticorrupción |
| [219.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/219.md) | Ley General del Sistema Nacional de Seguridad Pública |
| [220.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/220.md) | Ley General del Sistema para la Carrera de las Maestras y los Maestros |
| [221.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/221.md) | Ley General en Materia de Delitos Electorales |
| [222.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/222.md) | Ley General en Materia de Desaparición Forzada de Personas, Desaparición Cometida por Particulares y del Sistema Nacional de Búsqueda de Personas |
| [223.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/223.md) | Ley General para el Control del Tabaco |
| [224.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/224.md) | Ley General para la Atención y Protección a Personas con la Condición del Espectro Autista |
| [225.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/225.md) | Ley General para la Detección Oportuna del Cáncer en la Infancia y la Adolescencia |
| [226.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/226.md) | Ley General para la Igualdad entre Mujeres y Hombres |
| [227.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/227.md) | Ley General para la Inclusión de las Personas con Discapacidad |
| [228.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/228.md) | Ley General para la Prevención Social de la Violencia y la Delincuencia |
| [229.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/229.md) | Ley General para la Prevención y Gestión Integral de los Residuos |
| [230.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/230.md) | Ley General para Prevenir, Investigar y Sancionar la Tortura y Otros Tratos o Penas Crueles, Inhumanos o Degradantes |
| [231.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/231.md) | Ley General para Prevenir, Sancionar y Erradicar los Delitos en Materia de Trata de Personas y para la Protección y Asistencia a las Víctimas de estos Delitos |
| [232.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/232.md) | Ley General para Prevenir y Sancionar los Delitos en Materia de Secuestro, Reglamentaria de la fracción XXI del artículo 73 de la Constitución Política de los Estados Unidos Mexicanos |
| [233.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/233.md) | Ley Minera |
| [234.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/234.md) | Ley Monetaria de los Estados Unidos Mexicanos |
| [235.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/235.md) | Ley Nacional de Ejecución Penal |
| [236.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/236.md) | Ley Nacional de Extinción de Dominio |
| [237.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/237.md) | Ley Nacional de Mecanismos Alternativos de Solución de Controversias en Materia Penal |
| [238.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/238.md) | Ley Nacional del Registro de Detenciones |
| [239.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/239.md) | Ley Nacional del Sistema Integral de Justicia Penal para Adolescentes |
| [240.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/240.md) | Ley Nacional sobre el Uso de la Fuerza |
| [241.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/241.md) | Ley Orgánica de la Administración Pública Federal |
| [242.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/242.md) | Ley Orgánica de la Armada de México |
| [243.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/243.md) | Ley Orgánica de la Financiera Nacional de Desarrollo Agropecuario, Rural, Forestal y Pesquero |
| [244.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/244.md) | Ley Orgánica de la Procuraduría de la Defensa del Contribuyente |
| [245.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/245.md) | Ley Orgánica de la Procuraduría General de Justicia del Distrito Federal |
| [246.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/246.md) | Ley Orgánica de la Universidad Autónoma Agraria Antonio Narro |
| [247.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/247.md) | Ley Orgánica de la Universidad Autónoma Metropolitana |
| [248.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/248.md) | Ley Orgánica de la Universidad Nacional Autónoma de México |
| [249.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/249.md) | Ley Orgánica de los Tribunales Agrarios |
| [250.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/250.md) | Ley Orgánica de Nacional Financiera |
| [251.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/251.md) | Ley Orgánica de Sociedad Hipotecaria Federal |
| [252.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/252.md) | Ley Orgánica del Banco del Bienestar |
| [253.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/253.md) | Ley Orgánica del Banco Nacional de Comercio Exterior |
| [254.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/254.md) | Ley Orgánica del Banco Nacional de Obras y Servicios Públicos |
| [255.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/255.md) | Ley Orgánica del Banco Nacional del Ejército, Fuerza Aérea y Armada |
| [256.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/256.md) | Ley Orgánica del Centro Federal de Conciliación y Registro Laboral |
| [257.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/257.md) | Ley Orgánica del Congreso General de los Estados Unidos Mexicanos |
| [258.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/258.md) | Ley Orgánica del Consejo Nacional de Ciencia y Tecnología |
| [259.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/259.md) | Ley Orgánica del Ejército y Fuerza Aérea Mexicanos |
| [260.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/260.md) | Ley Orgánica del Instituto Nacional de Antropología e Historia |
| [261.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/261.md) | Ley Orgánica del Instituto Politécnico Nacional |
| [262.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/262.md) | Ley Orgánica del Poder Judicial de la Federación |
| [263.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/263.md) | Ley Orgánica del Seminario de Cultura Mexicana |
| [264.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/264.md) | Ley Orgánica del Tribunal Federal de Justicia Administrativa |
| [265.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/265.md) | Ley para Conservar la Neutralidad del País |
| [266.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/266.md) | Ley para Determinar el Valor de la Unidad de Medida y Actualización |
| [267.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/267.md) | Ley para el Desarrollo de la Competitividad de la Micro, Pequeña y Mediana Empresa |
| [268.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/268.md) | Ley para el Diálogo, la Conciliación y la Paz Digna en Chiapas |
| [269.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/269.md) | Ley para el uso y protección de la denominación y del emblema de la Cruz Roja |
| [270.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/270.md) | Ley para Impulsar el Incremento Sostenido de la Productividad y la Competitividad de la Economía Nacional |
| [271.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/271.md) | Ley para la Comprobación, Ajuste y Cómputo de Servicios de la Armada de México |
| [272.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/272.md) | Ley para la Comprobación, Ajuste y Cómputo de Servicios en el Ejército y Fuerza Aérea Mexicanos |
| [273.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/273.md) | Ley para la Depuración y Liquidación de Cuentas de la Hacienda Pública Federal |
| [274.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/274.md) | Ley para la Protección de Personas Defensoras de Derechos Humanos y Periodistas |
| [275.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/275.md) | Ley para la Transparencia, Prevención y Combate de Prácticas Indebidas en Materia de Contratación de Publicidad |
| [276.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/276.md) | Ley para la Transparencia y Ordenamiento de los Servicios Financieros |
| [277.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/277.md) | Ley para Regular las Actividades de las Sociedades Cooperativas de Ahorro y Préstamo |
| [278.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/278.md) | Ley para Regular las Agrupaciones Financieras |
| [279.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/279.md) | Ley para Regular las Instituciones de Tecnología Financiera |
| [280.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/280.md) | Ley para Regular las Sociedades de Información Crediticia |
| [281.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/281.md) | Ley que Aprueba la Adhesión de México al Convenio Constitutivo del Banco de Desarrollo del Caribe y su Ejecución |
| [282.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/282.md) | Ley que Crea el Fondo de Garantía y Fomento para la Agricultura, Ganadería y Avicultura |
| [283.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/283.md) | Ley que crea el Instituto Nacional de Bellas Artes y Literatura |
| [284.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/284.md) | Ley que Crea la Agencia de Noticias del Estado Mexicano |
| [285.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/285.md) | Ley que crea la Agencia Espacial Mexicana |
| [286.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/286.md) | Ley que crea la Universidad Autónoma Chapingo |
| [287.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/287.md) | Ley que Crea la Universidad del Ejército y Fuerza Aérea |
| [288.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/288.md) | Ley que Declara Reservas Mineras Nacionales los Yacimientos de Uranio, Torio y las demás Substancias de las cuales se Obtengan Isótopos Hendibles que puedan Producir Energía Nuclear |
| [289.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/289.md) | Ley que Establece Bases para la Ejecución en México, por el Poder Ejecutivo Federal, del Convenio Constitutivo de la Asociación Internacional de Fomento |
| [290.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/290.md) | Ley que Establece Bases para la Ejecución en México, por el Poder Ejecutivo Federal, del Convenio Constitutivo del Banco Interamericano de Desarrollo |
| [291.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/291.md) | Ley Reglamentaria de la Fracción V del Artículo 76 de la Constitución General de la República |
| [292.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/292.md) | Ley Reglamentaria de la fracción VI del artículo 76 de la Constitución Política de los Estados Unidos Mexicanos |
| [293.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/293.md) | Ley Reglamentaria de la Fracción XIII Bis del Apartado B, del Artículo 123 de la Constitución Política de los Estados Unidos Mexicanos |
| [294.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/294.md) | Ley Reglamentaria de la Fracción XVIII del Artículo 73 Constitucional, en lo que se Refiere a la Facultad del Congreso para Dictar Reglas para Determinar el Valor Relativo de la Moneda Extranjera |
| [295.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/295.md) | Ley Reglamentaria de las Fracciones I y II del Artículo 105 de la Constitución Política de los Estados Unidos Mexicanos |
| [296.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/296.md) | Ley Reglamentaria del Artículo 27 Constitucional en Materia Nuclear |
| [297.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/297.md) | Ley Reglamentaria del Artículo 3o. de la Constitución Política de los Estados Unidos Mexicanos, en materia de Mejora Continua de la Educación |
| [298.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/298.md) | Ley Reglamentaria del Artículo 5o. Constitucional, relativo al ejercicio de las profesiones en la Ciudad de México |
| [299.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/299.md) | Ley Reglamentaria del artículo 6o., párrafo primero, de la Constitución Política de los Estados Unidos Mexicanos, en materia del Derecho de Réplica |
| [300.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/300.md) | Ley Reglamentaria del Servicio Ferroviario |
| [301.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/301.md) | Ley sobre Cámaras Agrícolas, que en lo sucesivo se denominarán Asociaciones Agrícolas |
| [302.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/302.md) | Ley sobre Delitos de Imprenta |
| [303.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/303.md) | Ley Sobre el Contrato de Seguro |
| [304.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/304.md) | Ley sobre el Escudo, la Bandera y el Himno Nacionales |
| [305.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/305.md) | Ley sobre Elaboración y Venta de Café Tostado |
| [306.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/306.md) | Ley Sobre la Aprobación de Tratados Internacionales en Materia Económica |
| [307.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/307.md) | Ley sobre la Celebración de Tratados |
| [308.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/308.md) | Ley sobre Refugiados, Protección Complementaria y Asilo Político |
| [309.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/309.md) | Ordenanza General de la Armada |
| [310.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/310.md) | Presupuesto de Egresos de la Federación para el Ejercicio Fiscal 2022 |
| [311.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/311.md) | Presupuesto de Egresos de la Federación para el Ejercicio Fiscal 2021 |
| [312.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/312.md) | Reglamento de la Cámara de Diputados |
| [313.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/313.md) | Reglamento del Senado de la República |
| [314.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/314.md) | Reglamento para el Gobierno Interior del Congreso General de los Estados Unidos Mexicanos |
| [315.md](https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/315.md) | Decreto por el que se establece el Horario Estacional que se aplicará en los Estados Unidos Mexicanos |