# frozen_string_literal: true

require 'json'
require 'open-uri'

def renumber(id)
  ceros = '0' * (3 - id.to_s.length)
  ceros = '' if ceros.empty?
  "#{ceros}#{id}"
end

def convert(url, path)
  IO.copy_stream(URI.parse(url).open, "#{path}.doc")
  system("abiword --to=docx #{path}.doc")
  system("pandoc #{path}.docx -o #{path}.md")
  File.delete("#{path}.doc")
  File.delete("#{path}.docx")
rescue StandardError => e
  msg = "[#{Time.now}] Algo salió mal con la conversión de #{path}.doc\n#{e}"
  log = '../log'
  old = File.read(log)
  File.open(log, 'w') { |f| f.write("#{old}\n#{msg}".strip) }
end

Dir.chdir('src')

readme = ["# Diff de las legislaciones federales vigentes de México\n", '| Directorio | Legislación |', '|---|---|']
legislaciones = JSON.parse(URI.parse('https://clientes.programando.li/bdo/v2/legislaciones.json').open.read)

legislaciones.each do |legislacion|
  src = renumber(legislacion['id'])
  doc = legislacion['links'][2]['url']
  git = 'https://gitlab.com/programando-libreros/juridico/leyes-mx-diff/-/blob/master/src/'
  convert(doc, src)
  readme.push("| [#{src}.md](#{git}#{src}.md) | #{legislacion['name']} |")
end

File.open('../README.md', 'w') { |f| f.write(readme.join("\n")) }

system("git add . && git commit -m 'Actualización' && git push origin master")
